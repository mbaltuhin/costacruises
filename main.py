import datetime
import os

import requests

import xlsxwriter


def write_file_to_excell(array):
    userhome = os.path.expanduser('~')
    now = datetime.datetime.now()
    path_to_file = userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(
        now.day) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '-Costa Cruises.xlsx'
    if not os.path.exists(userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(
            now.month) + '-' + str(now.day)):
        os.makedirs(
            userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(now.day))
    workbook = xlsxwriter.Workbook(path_to_file)

    worksheet = workbook.add_worksheet()
    worksheet.set_column("A:A", 15)
    worksheet.set_column("B:B", 25)
    worksheet.set_column("C:C", 10)
    worksheet.set_column("D:D", 25)
    worksheet.set_column("E:E", 20)
    worksheet.set_column("F:F", 30)
    worksheet.set_column("G:G", 20)
    worksheet.set_column("H:H", 50)
    worksheet.set_column("I:I", 20)
    worksheet.set_column("J:J", 20)
    worksheet.set_column("K:K", 20)
    worksheet.set_column("L:L", 20)
    worksheet.set_column("M:M", 25)
    worksheet.set_column("N:N", 20)
    worksheet.set_column("O:O", 20)
    worksheet.set_column("P:P", 21)
    worksheet.write('A1', 'DestinationCode')
    worksheet.write('B1', 'DestinationName')
    worksheet.write('C1', 'VesselID')
    worksheet.write('D1', 'VesselName')
    worksheet.write('E1', 'CruiseID')
    worksheet.write('F1', 'CruiseLineName')
    worksheet.write('G1', 'ItineraryID')
    worksheet.write('H1', 'BrochureName')
    worksheet.write('I1', 'NumberOfNights')
    worksheet.write('J1', 'SailDate')
    worksheet.write('K1', 'ReturnDate')
    worksheet.write('L1', 'InteriorBucketPrice')
    worksheet.write('M1', 'OceanViewBucketPrice')
    worksheet.write('N1', 'BalconyBucketPrice')
    worksheet.write('O1', 'SuiteBucketPrice')
    worksheet.write('P1', 'PortList')
    col = 0
    row = 1
    for result in array:
        for item in result:
            if col == {11, 12, 13, 14}:
                try:
                    worksheet.write_number(row, col, item)
                except ValueError:
                    worksheet.write_string(row, col, str(item))
            elif col == {9, 10}:
                worksheet.write_datetime(row, col, item)
            else:
                worksheet.write_string(row, col, str(item))
            col += 1
        row += 1
        col = 0
    workbook.close()


headers = {
    "country": "US",
    "locale": "en_US",
    "accept": "application/json"
}

session = requests.session()
parsed_results = []
response = session.get(
    "https://www.costacruises.com/search/costa_en_US/cruisesearch?&fq=(price_USD_anonymous:[1%20TO%20*]%20)&rows=1000",
    headers=headers).json()
total_results = response["results"]
counter = 0
for result in response["searchResults"]:
    cruise_id = result["cruiseId"]
    destination_id = result["destinationIds"]
    itinerary_id = result["itineraryId"]
    destination_name = result["destinationName"]
    duration = result["duration"]
    is_sold_out = result["isSoldOut"]
    ports = result["portsOfCall"]
    ship_name = result["shipName"]
    ship_id = result["shipId"]
    departure_date_formatted = datetime.datetime.strptime(result['departureDate'], '%Y-%m-%dT%H:%M:%SZ').date()
    arrival_date_formatted = datetime.datetime.strptime(result['arrivalDate'], '%Y-%m-%dT%H:%M:%SZ').date()
    interior_price = result["price_USD_Acquamarina"].split(".")[0]
    oceanview_price = result["price_USD_Ambra"].split(".")[0]
    balcony_price = result["price_USD_Corallo"].split(".")[0]
    suite_price = result["price_USD_Perla"].split(".")[0]

    parsed_results.append(
        [destination_id, destination_name, ship_id, ship_name, cruise_id, "Costsa Cruises", itinerary_id, "",
         duration, departure_date_formatted, arrival_date_formatted, interior_price, oceanview_price, balcony_price, suite_price, ports])

write_file_to_excell(parsed_results)

input("Press ENTER key twice to continue...")
